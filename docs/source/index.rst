Simple Documentation Tutorial: Doctut
=====================================

Test Header
===========

Text explaining some complicated stuff.::

  print("Hello")
  >> Hello

Guide
^^^^^

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   license
   help



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
